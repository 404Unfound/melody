package me.Unfound.melody.listeners;

import me.Unfound.melody.MusicUI;
import me.Unfound.melody.SongPlayer.PlaySong;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GUIListener implements Listener {

    PlaySong ps = new PlaySong();

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Inventory inv = e.getInventory();
        Player p = (Player) e.getWhoClicked();
        ItemStack item = e.getCurrentItem();
        int page = Integer.parseInt(inv.getItem(0).getItemMeta().getLocalizedName());
        if (item != null && item.getType() != null && e.getView().getTitle().equalsIgnoreCase("Music Player")) {
            if (item.getType().equals(Material.GREEN_STAINED_GLASS_PANE)) {
                if (item.getItemMeta().hasLocalizedName()) {
                    e.setCancelled(true);
                    p.closeInventory();
                    new MusicUI(p, page - 1);
                } else {
                    e.setCancelled(true);
                    p.closeInventory();
                    new MusicUI(p, page + 1);
                }
            } else if (item.getType().equals(Material.MUSIC_DISC_WARD)) {
                ps.playSong(p, ChatColor.stripColor(item.getItemMeta().getDisplayName()));
            }
        }

        e.setCancelled(true);


    }

}
