package me.Unfound.melody.utils;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class PageUtils {

    public static List<ItemStack> getPageItem(List<ItemStack> items, int page, int spaces) {
        int upperBound = page * spaces; //1*45
        int lowerBound = upperBound - spaces;

        List<ItemStack> newItems = new ArrayList<>();
        for (int i = lowerBound; i < upperBound; i++) {
            newItems.add(items.get(i - 1));
        }
        return newItems;
    }

    public static boolean isPageValid(List<ItemStack> items, int page, int spaces) {
        if (page <= 0) {
            return false;
        }
        int upperBound = page * spaces;
        int lowerBound = upperBound - spaces;
        return items.size() > lowerBound;
    }

}
