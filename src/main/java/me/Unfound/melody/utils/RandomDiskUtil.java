package me.Unfound.melody.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class RandomDiskUtil {

    public ItemStack RandomDiskUtil() {
        List<ItemStack> disk = new ArrayList<>();
        disk.add(new ItemStack(Material.MUSIC_DISC_13));
        disk.add(new ItemStack(Material.MUSIC_DISC_CAT));
        disk.add(new ItemStack(Material.MUSIC_DISC_BLOCKS));
        disk.add(new ItemStack(Material.MUSIC_DISC_CHIRP));
        disk.add(new ItemStack(Material.MUSIC_DISC_FAR));
        disk.add(new ItemStack(Material.MUSIC_DISC_MALL));
        disk.add(new ItemStack(Material.MUSIC_DISC_MELLOHI));
        disk.add(new ItemStack(Material.MUSIC_DISC_STAL));
        disk.add(new ItemStack(Material.MUSIC_DISC_WAIT));
        disk.add(new ItemStack(Material.MUSIC_DISC_WARD));
        int max = disk.size();
        int rand = (int) (Math.random() * ((max) + 1));
        return disk.get(rand);
    }

}
