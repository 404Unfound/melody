package me.Unfound.melody;

import me.Unfound.melody.SongPlayer.PlaySong;
import me.Unfound.melody.utils.PageUtils;
import me.Unfound.melody.utils.RandomDiskUtil;
import me.Unfound.melody.utils.Utilities;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class MusicUI {

    RandomDiskUtil r = new RandomDiskUtil();

    public MusicUI(Player p, int page) {

        Inventory inv = Bukkit.createInventory(null, 54, "Music Player");

        List<ItemStack> allItems = new ArrayList<>();
        for (String song : PlaySong.songList) {
            ItemStack item = new ItemStack(Material.MUSIC_DISC_WARD);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.AQUA + song);
            item.setItemMeta(itemMeta);
            allItems.add(item);
        }

        for (ItemStack item : PageUtils.getPageItem(allItems, page, 45)) {
            inv.setItem(inv.firstEmpty(), item);
        }

        ItemStack left;
        ItemStack right;
        if (PageUtils.isPageValid(allItems, page - 1, 45)) {
            left = new ItemStack(Material.GREEN_STAINED_GLASS_PANE);
            ItemMeta lm = left.getItemMeta();
            lm.setDisplayName(Utilities.colorize("&a&lGo Left"));
            lm.setLocalizedName(page + "");
            left.setItemMeta(lm);
        } else {
            left = new ItemStack(Material.RED_STAINED_GLASS_PANE);
            ItemMeta lm = left.getItemMeta();
            lm.setDisplayName(Utilities.colorize("&c&l" + "\uD83D\uDEC7"));
            lm.setLocalizedName(page + "");
            left.setItemMeta(lm);
        }
        if (PageUtils.isPageValid(allItems, page + 1, 45)) {
            right = new ItemStack(Material.GREEN_STAINED_GLASS_PANE);
            ItemMeta rm = right.getItemMeta();
            rm.setDisplayName(Utilities.colorize("&a&lGo Right"));
            right.setItemMeta(rm);
        } else {
            right = new ItemStack(Material.RED_STAINED_GLASS_PANE);
            ItemMeta rm = right.getItemMeta();
            rm.setDisplayName(Utilities.colorize("&c&l" + "\uD83D\uDEC7"));
            right.setItemMeta(rm);
        }

        inv.setItem(0, left);
        inv.setItem(8, right);

        p.openInventory(inv);

    }

}
