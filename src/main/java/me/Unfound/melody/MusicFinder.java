package me.Unfound.melody;

import me.Unfound.melody.SongPlayer.PlaySong;

import java.io.File;

public class MusicFinder {

    public File musicFinder(String song) {
        String s = song.toLowerCase();
        File songFile;
        for (String st : PlaySong.songList) {
            if (st.equalsIgnoreCase(s)) {
                songFile = new File(Melody.getInstance().getDataFolder() + File.separator + "songs" + File.separator + st);
                return songFile;
            }
        }
        return null;
    }


}
