package me.Unfound.melody.SongPlayer;

import com.xxmicloxx.NoteBlockAPI.NoteBlockAPI;
import com.xxmicloxx.NoteBlockAPI.model.Song;
import com.xxmicloxx.NoteBlockAPI.songplayer.RadioSongPlayer;
import com.xxmicloxx.NoteBlockAPI.utils.NBSDecoder;
import me.Unfound.melody.Melody;
import me.Unfound.melody.MusicFinder;
import me.Unfound.melody.utils.Utilities;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

public class PlaySong {

    public static Map<Player, String> music = new HashMap<>();
    public static List<String> songList = new ArrayList<>();
    public MusicFinder mf = new MusicFinder();

    public void playSong(Player player, String s) {
        try {
            NoteBlockAPI.stopPlaying(player);
            Song song = NBSDecoder.parse(mf.musicFinder(s));
            RadioSongPlayer rp = new RadioSongPlayer(song);
            rp.addPlayer(player);
            rp.setPlaying(true);
            music.put(player, s.replace(".nbs", ""));
            Utilities.tell(player, "&aCurrently playing: &2" + s.replace(".nbs", ""));
        } catch (Exception e) {
            Utilities.tell(player, "&cTrack Not Found!");
        }
    }

    public void registerList() {
        File f = new File(Melody.getInstance().getDataFolder() + File.separator + "songs");

        FilenameFilter textFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".nbs");
            }
        };

        int songNumb = 0;

        File[] files = f.listFiles(textFilter);
        Utilities.logConsole(Level.INFO, "+============[SongLoader]============+");
        for (File file : files) {
            if (file.isDirectory()) {
                //NOTHING
            } else {
                Utilities.logConsole(Level.INFO, "Song Found: " + file.getName());
                songList.add(file.getName());
                songNumb++;
            }

        }
        Utilities.logConsole(Level.INFO, "Total of songs: " + songNumb);
        Utilities.logConsole(Level.INFO, "+============[SongLoader]============+");

    }

}
