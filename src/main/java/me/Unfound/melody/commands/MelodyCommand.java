package me.Unfound.melody.commands;

import com.xxmicloxx.NoteBlockAPI.NoteBlockAPI;
import me.Unfound.melody.MusicUI;
import me.Unfound.melody.SongPlayer.PlaySong;
import me.Unfound.melody.utils.Utilities;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class MelodyCommand extends BukkitCommand {

    private final PlaySong playsong = new PlaySong();

    public MelodyCommand() {
        super("music");
        setAliases(Arrays.asList("melody", "m"));
        setUsage("/music <command>");
        setDescription("Default command for Melody");
    }

    public boolean execute(CommandSender s, String arg, String[] args) {
        if (!(s instanceof Player)) {
            s.sendMessage("You must be a player to run this command");
            return true;
        }
        Player p = (Player) s;
        if (args.length == 0 || args[0].equalsIgnoreCase("menu")) {
            if (PlaySong.songList.size() == 0) {
                Utilities.tell(p, Utilities.colorize("&cNo Songs Loaded"));
                return true;
            }
            new MusicUI(p, 1);
            return true;
        }
        if (args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")) {
            Utilities.tell(p, "&a+================&2[Help]&a================+");
            Utilities.tell(p, "&a/melody - Open the GUI.");
            Utilities.tell(p, "&a/melody help/? - This menu");
            Utilities.tell(p, "&a/melody list - List of loaded songs");
            Utilities.tell(p, "&a/melody play <song>");
            Utilities.tell(p, "&a/melody stop - Stop the current song");
            Utilities.tell(p, "&a+================&2[Help]&a================+");
            return true;
        }
        if (args[0].equalsIgnoreCase("play")) {
            StringBuilder sb = new StringBuilder();
            if (args.length >= 3) {
                for (int i = 1; i < args.length; ) {
                    sb.append(args[i]).append(" ");
                    i++;
                }
                String song = sb.toString().replaceAll("\\s+$", "");
                if (song.contains(".nbs")) {
                    playsong.playSong(p, song);
                } else {
                    playsong.playSong(p, song + ".nbs");
                }
            } else {

                if (args[1].contains(".nbs")) {
                    playsong.playSong(p, args[1]);
                } else {
                    playsong.playSong(p, args[1] + ".nbs");
                }
            }
            return true;
        }
        if (args[0].equalsIgnoreCase("list")) {
            Utilities.tell(p, "&a+====================&2[Music List]&a====================+");
            for (String str : PlaySong.songList) {
                Utilities.tell(p, "&a" + str.replace(".nbs", ""));
            }
            Utilities.tell(p, "&a+====================&2[Music List]&a====================+");
            return true;
        }

        if (args[0].equalsIgnoreCase("stop")) {
            if (NoteBlockAPI.isReceivingSong(p)) {
                NoteBlockAPI.stopPlaying(p);
                return true;
            }
            Utilities.tell(p, "&cNot playing any songs currently");
            return true;
        }


        Utilities.tell(p, "&cUnknown command, please execute /melody help for a list of commands!");
        return true;
    }

}
