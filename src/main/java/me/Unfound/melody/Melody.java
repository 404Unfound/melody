package me.Unfound.melody;

import me.Unfound.melody.SongPlayer.PlaySong;
import me.Unfound.melody.commands.MelodyCommand;
import me.Unfound.melody.listeners.GUIListener;
import me.Unfound.melody.listeners.SongListeners;
import me.Unfound.melody.utils.Utilities;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

public final class Melody extends JavaPlugin implements Listener {

    public static List<ItemStack> musicItems = new ArrayList<>();
    private static Melody instance;
    private final PlaySong playsong = new PlaySong();
    PluginDescriptionFile pdf = this.getDescription();
    FileConfiguration config = this.getConfig();

    public static Melody getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        boolean NoteBlockAPI = true;
        registerCommands(new MelodyCommand());
        registerEvents(new SongListeners(), new GUIListener());
        this.saveDefaultConfig();
        File songFiles;
        try {
            songFiles = new File(getDataFolder() + File.separator + "songs");
            if (!songFiles.exists()) {
                songFiles.mkdirs();
            }
        } catch (SecurityException e) {
            Utilities.logConsole(Level.SEVERE, "Unable to create song folders! You will have to manually create the folder 'songs in the plugin's root'!");
            return;
        }

        new Updater(73128) {

            @Override
            public void onUpdateAvailable() {
                Utilities.logConsole(Level.WARNING, getUpdateMessage());
            }
        }.runTaskAsynchronously(this);

        playsong.registerList();
        for (String s : playsong.songList) {
            ItemStack cd = new ItemStack(Material.MUSIC_DISC_CAT);
            ItemMeta cdMeta = cd.getItemMeta();
            cdMeta.setDisplayName(Utilities.colorize("&b&l" + s.replace(".nbs", "")));
            cdMeta.setLore(Arrays.asList(Utilities.colorize("&aClick here to play &b" + s.replace(".nbs", ""))));
            cd.setItemMeta(cdMeta);
            musicItems.add(cd);
        }

    }

    @Override
    public void onDisable() {
        instance = null;
        PlaySong.music.clear();
    }

    public void registerCommands(Command... cmd) {
        for (Command command : cmd) {
            Utilities.registerCommand(command);
        }
    }

    public void registerEvents(Listener... listener) {
        PluginManager pm = getServer().getPluginManager();

        for (Listener l : listener) {
            pm.registerEvents(l, this);
        }
    }
}

