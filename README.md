Melody
======
Melody is an open-source plugin permitting server owners to play music in minecraft. Using NoteBlockAPI, Melody allows
server owner to create or download .nbs files and playing them in-game! It is lag-free and revolutionary!

Creating .nbs files
-------------------
To create .nbs files, you need to download [NoteBlockStudio](https://hielkeminecraft.github.io/OpenNoteBlockStudio/) and follow the steps on their wiki.

Contributing
------------

We happily accept contributions, especially through merge requests on GitLab.
Submissions must be licensed under the GNU Lesser General Public License v3.

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for important guidelines to follow.

Links
-----
* [Discord](https://discord.gg/Jp7zqNZ)
* [Twitter](https://twitter.com/SimonChambs00)